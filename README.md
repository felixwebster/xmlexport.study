![Большой котейка](bigCat.jpg)

# Учимся работать с большими xml-файлами

Основная проблема работы с xml-файлами, это лимиты использования памяти и производительность. Чтобы решить эти две проблемы, надо работать с xml в потоке

#### Поставим задачу считать большой файл (200MB+)

Вот пример файла, который будем рассматривать:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<catalog>
    <product>
        <name>Очень крутые кроссовки</name>
        <description>Чёткие кроссовки для не менее чётких пацанов</description>
        <color>Вроде красные</color>
        <size>39</size>
        <price>49000 RUB</price>
    </product>
    <product>
        <name>Дешёвые кроссовки</name>
        <description>Вариант подешевше уже для не очень крутых пацанов</description>
        <color>Зелёные</color>
        <size>40</size>
        <price>15000 RUB</price>
    </product>
</catalog>
```

Если в файле будет описано немного товаров, то нам подойдёт такой код для решения нашей задачи:

```php
// Чтение с помощью SimpleXML
$loadedFile = simplexml_load_file($fileName);
// Можно ещё так
$loadedFile = simplexml_load_string(file_get_contents($fileName));

// Выведем название и цену
foreach ($loadedFile->product as $objProduct) {
    echo "{$objProduct->name} за {$objProduct->price}\n";
}


// Чтение с помощью DOM
$reader = new DOMDocument();
$reader->load($fileName);

// Выведем название и цену
$productsCollection = $reader->getElementsByTagName('product');

/** @var \DOMElement $item */
foreach ($productsCollection as $item) {
    $arProduct = [
        'name' => $item->getElementsByTagName('name')->item(0)->nodeValue,
        'price' => $item->getElementsByTagName('price')->item(0)->nodeValue,
    ];

    echo "{$arProduct['name']} за {$arProduct['price']}\n";
}
```

Но если запустить парсинг big.xml, то получите вот такую ошибку: `PHP Fatal error:  Allowed memory size of 134217728 bytes exhausted (tried to allocate 696552808 bytes)`.

А если запустить парсинг через cli без ограничения памяти, как я, то у вас зависнет курсор, остановится музыка, а компьютер начнёт сильно шуметь куллерами.

Вот как читают большие файлы крутые бородатые дяди:

```php
// Инициализируем ридер и открываем файл
$reader = new XMLReader();
$reader->open('large.xml');

// Тут будет описание товара
$arProduct = [];

// Читаем файл до конца
// Инициализируем ридер и открываем файл
$reader = new XMLReader();
$reader->open('large.xml');

// Тут будет описание товара
$arProduct = [];

// Читаем файл до конца
while ($reader->read()) {
    // Если указатель на элементе, то смотрим его название
    if ($reader->nodeType == XMLReader::ELEMENT) {
        if ($reader->name === 'catalog') {
            // Если catalog, то просто рподолжаем
            continue;
        } elseif ($reader->name === 'product') {
            // Если это товар, то печатаем предыдущий
            // и сбрасываем массив товара
            if (!empty($arProduct)) {
                echo "{$arProduct['name']} за {$arProduct['price']}\n";
            }

            $arProduct = [];
        } elseif ($reader->name === 'name') {
            // Если это название, то записываем его в массив
            $arProduct['name'] = $reader->readString();
        } elseif ($reader->name === 'price') {
            // Цену тоже
            $arProduct['price'] = $reader->readString();
        }
    }
}


$reader = new XMLReader();
$reader->open('large.xml');

// Вот вариант удобнее, короче,
// но менее производительный (51c против 86с на 1000-е элементов)
while ($reader->read()) {
    if (($reader->nodeType == XMLReader::ELEMENT)
        && ($reader->name === 'product')
    ) {
        // Если product, то получаем xml узла и парсим его
        // с помощью SimpleXML
        $objProduct = simplexml_load_string($reader->readOuterXml());
        echo "{$objProduct->name} за {$objProduct->price}\n";
    }
}
```

Идея в том, чтобы не хранить в памяти кучу ненужной информации. Разработчики XMLReader уже позаботились о потоковом чтении файла. Нам остаётся только получать элементы в цикле, смотреть что вернулось и делать с данными что нужно.

Первый вариант не такой удобный, как SimpleXML, но зато помогает уложиться в скромные ресурсы сервера

#### А что на счёт записи?

С записью идея та же, надо держать в памяти как можно меньше ненужных данных

С небольшими файлами прокатит довольно удобный вариант с использование DOM и/или SimpleXML:

```php
// С помощью DOM

// Создаём документ и корневой элемент каталог
$xmlFile = new DOMDocument('1.0', 'UTF-8');
$catalogNode = $xmlFile->appendChild($xmlFile->createElement('catalog', ''));

// Пишем товары
for ($i = 1; $i < 5; $i++) {
    $arProduct = getRandomProduct($exampleData);

    // Создаём ноду product
    $productNode = $catalogNode->appendChild($xmlFile->createElement('product'));

    // Создаём ноды name, color, size, price
    $productNode->appendChild($xmlFile->createElement('name', "{$arProduct['prefix']} {$arProduct['name']} by {$arProduct['developer']}"));
    $productNode->appendChild($xmlFile->createElement('color', $arProduct['color']));
    $productNode->appendChild($xmlFile->createElement('size', $arProduct['size']));
    $productNode->appendChild($xmlFile->createElement('price', $arProduct['price']));
}

// Надо выставить перед сохранением,
// чтобы получить красивенький файл с отступами
$xmlFile->preserveWhiteSpace = true;
$xmlFile->formatOutput = true;

file_put_contents('../output/smallByDOM.xml', $xmlFile->saveXML());


// С помощью SimpleXML

// Создаём документ и корневой элемент каталог
$xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><catalog/>');

// Пишем товары
for ($i = 1; $i < 5; $i++) {
    $arProduct = getRandomProduct($exampleData);

    // Создаём ноду product
    $productNode = $xml->addChild('product');

    $productNode->addChild('name', "{$arProduct['prefix']} {$arProduct['name']} by {$arProduct['developer']}");
    $productNode->addChild('name', $arProduct['color']);
    $productNode->addChild('name', $arProduct['size']);
    $productNode->addChild('name', $arProduct['price']);
}

// Отформатировать вывод средствами SimpleXML нельзя
$xml->saveXML('../output/smallBySimpleXML.xml');

// Зато можно скормить XML DOM'у и сохранить им
$dom = dom_import_simplexml($xml)->ownerDocument;
$dom->formatOutput = true;
file_put_contents('../output/smallBySimpleXMLWithDOM.xml', $dom->saveXML());
```

Большой файл таким способом уже не запишешь, надо использовать менее удобный XMLWriter.
Его плюс в поддержке потоковой записи.

```php
// Создаём райтер
$xmlWriter = new XMLWriter();
$xmlWriter->openMemory();

// Выставляем правила форматирования
$xmlWriter->setIndent(true);
$xmlWriter->setIndentString('	');

// Начинаем документ и корневой элемент каталог
$xmlWriter->startDocument('1.0', 'UTF-8');
$xmlWriter->startElement('catalog');

// Пишем товары
for ($i = 0; $i <= 10000000; ++$i) {
    $arProduct = getRandomProduct($exampleData);

    // Открываем ноду product
    $xmlWriter->startElement('product');

        // Создаём ноды name, color, size, price
        $xmlWriter->writeElement('name', "{$arProduct['prefix']} {$arProduct['name']} by {$arProduct['developer']}");
        $xmlWriter->writeElement('color', $arProduct['color']);
        $xmlWriter->writeElement('size', $arProduct['size']);
        $xmlWriter->writeElement('price', $arProduct['price']);

    // Закрываем ноду product
    $xmlWriter->endElement();

    // Каждую тысячу итераций скидываем изменения в файл
    // и освобождаем память
    if (0 == $i % 1000) {
        file_put_contents('../output/big.xml', $xmlWriter->flush(true), FILE_APPEND);
    }
}

// Закрываем элемент catalog
$xmlWriter->endElement();

// Скидываем последние изменения в файл
file_put_contents('../output/big.xml', $xmlWriter->flush(true), FILE_APPEND);
```

#### Что с многошаговостью?

Интернеты молчат по этому поводу. У меня идей на этот счёт не очень много.
С записью всё ещё более-менее понятно, просто выставить смещение при выборке элементов из БД, например, и записывать конец файла по условию окончания выборки.

С чтением сложнее. Можно запоминать порядковый номер текущего элемента, а при следующем запуске выполнять ```XMLReader::next()``` столько раз, сколько было обработано элементов. Но, по-моему, несколько костыльный вариант

#### Как быть со сложными выборками из БД?

Такой вопрос может возникнуть при импорте офферов, когда в выводе присутствует информация о товарах.

Какую-нибудь карту соответствия КодЦвета-Цвет, не занимающую много памяти, мы можем держать всегда по рукой.

С массивом товаров для офферов мы так поступить не можем. В таком случае, можно написать сложный SQL-запрос с JOIN'ами для выборки всех необходимых данных сразу.

Но есть другой способ - можно сделать два запроса. Один для офферов, с сортировкой по id родителя. Второй для товаров, с сортировкой по id самого товара.

Алгоритм такой:

*  Выбираем товар, сохраняем о нём информацию
*  Выбираем офферы и записываем всю необходимую информацию до тех пор, пока не изменится id родителя
*  При изменении id родителя выбираем следующий товар

#### А если мне не надо сохранять файл на диск, а надо отдавать в браузер?

Надо просто заменить операцию записи в файл, на операцию вывода в браузер и выставить несколько заголовков.

Вот такой, чтобы отдать файл прямо в браузер:

```php
header('Content-Type: application/xml');
```

А такие, чтобы скачать результат файлом:

```php
header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename="example.xml"');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
```

Если хотим отдать готовый XML с помощью readfile, то можно добавить:

```php
header('Content-Length: ' . filesize('ИмяФайла'));
```
 
При отдаче большого файла напрямую, следует помнить о тайм-ауте сервера. Генерация XML может просто оборваться и пользователь получит неполный файл.

Так-же не забудь выполнить ```ob_clean()``` перед выдачей файла, чтобы очистить буфер вывода

#### Для практики:

*  Можно сгенерировать [Yandex фид для электронной торговли](https://yandex.ru/support/partnermarket/export/yml.html)
*  Или [Google фид RSS 2.0](https://support.google.com/merchants/answer/160589) для Google Merchant Center