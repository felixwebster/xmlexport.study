<?php
/**
 * Created for internal use only
 * User: Felix Antufiev [felix@oneway.su]
 */

ini_set('memory_limit', '32M');

$fileName = realpath(__DIR__ . '/../files/large.xml');

// Инициализируем ридер и открываем файл
$reader = new XMLReader();
$reader->open($fileName);

// Тут будет описание товара
$arProduct = [];

// Читаем файл до конца
while ($reader->read()) {
    // Если указатель на элементе, то смотрим его название
    if ($reader->nodeType == XMLReader::ELEMENT) {
        if ($reader->name === 'catalog') {
            // Если catalog, то просто продолжаем
            continue;
        } elseif ($reader->name === 'product') {
            // Если это товар, то печатаем предыдущий
            // и сбрасываем массив товара
            if (!empty($arProduct)) {
                echo "{$arProduct['name']} за {$arProduct['price']}\n";
            }

            $arProduct = [];
        } elseif ($reader->name === 'name') {
            // Если это название, то записываем его в массив
            $arProduct['name'] = $reader->readString();
        } elseif ($reader->name === 'price') {
            // Цену тоже
            $arProduct['price'] = $reader->readString();
        }
    }
}


$reader = new XMLReader();
$reader->open($fileName);

// Вот вариант удобнее, короче,
// но менее производительный (51c против 86с на 1000 элементов)
while ($reader->read()) {
    if (($reader->nodeType == XMLReader::ELEMENT)
        && ($reader->name === 'product')
    ) {
        // Если product, то получаем xml узла и парсим его
        // с помощью SimpleXML
        $objProduct = simplexml_load_string($reader->readOuterXml());
        echo "{$objProduct->name} за {$objProduct->price}\n";
    }
}
