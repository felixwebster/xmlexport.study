<?php
/**
 * Created for internal use only
 * User: Felix Antufiev [felix@oneway.su]
 */

ini_set('memory_limit', '32M');

$fileName = realpath(__DIR__ . '/../files/small.xml');

// Чтение с помощью SimpleXML
$loadedFile = simplexml_load_file($fileName);
// Можно ещё так
$loadedFile = simplexml_load_string(file_get_contents($fileName));

// Выведем название и цену
foreach ($loadedFile->product as $objProduct) {
    echo "{$objProduct->name} за {$objProduct->price}\n";
}


// Чтение с помощью DOM
$reader = new DOMDocument();
$reader->load($fileName);

// Выведем название и цену
$productsCollection = $reader->getElementsByTagName('product');

/** @var \DOMElement $item */
foreach ($productsCollection as $item) {
    $arProduct = [
        'name' => $item->getElementsByTagName('name')->item(0)->nodeValue,
        'price' => $item->getElementsByTagName('price')->item(0)->nodeValue,
    ];

    echo "{$arProduct['name']} за {$arProduct['price']}\n";
}
