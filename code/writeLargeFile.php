<?php
/**
 * Created for internal use only
 * User: Felix Antufiev [felix@oneway.su]
 */

ini_set('memory_limit', '32M');

$exampleData = [
    'prefix' => ['Great', 'Fantastic', 'Unbelievable', 'Exquisite', 'Wonderful'],
    'name' => ['sneakers', 'boots', 'felt boots', 'shoes', 'loafers'],
    'developer' => ['Adidas', 'Gucci', 'Gosha Rubchinskiy', 'Pharrell Williams', 'Balenciaga'],
    'color' => ['Red', 'Orange', 'Yellow', 'Blue', 'Pink'],
    'size' => [38, 39, 40, 41, 42],
    'price' => ['32 000 RUB', '6 000 RUB', '13 500 RUB', '4 900 RUB', '12 990 RUB'],
];

function getRandomProduct(array &$exampleData)
{
    return array_map(function ($item) {
        return $item[rand(0, count($item) - 1)];
    }, $exampleData);
}

// С помощью XMLWriter

if (file_exists('../output/large.xml')) {
    unlink('../output/large.xml');
}

// Создаём райтер
$xmlWriter = new XMLWriter();
$xmlWriter->openMemory();

// Выставляем правила форматирования
$xmlWriter->setIndent(true);
$xmlWriter->setIndentString('	');

// Начинаем документ и корневой элемент каталог
$xmlWriter->startDocument('1.0', 'UTF-8');
$xmlWriter->startElement('catalog');

// Пишем товары
for ($i = 0; $i <= 10000000; $i++) {
    $arProduct = getRandomProduct($exampleData);

    // Открываем ноду product
    $xmlWriter->startElement('product');

    // Создаём ноды name, color, size, price
    $xmlWriter->writeElement('name', "{$arProduct['prefix']} {$arProduct['name']} by {$arProduct['developer']}");
    $xmlWriter->writeElement('color', $arProduct['color']);
    $xmlWriter->writeElement('size', $arProduct['size']);
    $xmlWriter->writeElement('price', $arProduct['price']);

    // Закрываем ноду product
    $xmlWriter->endElement();

    // Каждую тысячу итераций скидываем изменения в файл
    // и освобождаем память
    if (0 == $i % 1000) {
        file_put_contents('../output/large.xml', $xmlWriter->flush(true), FILE_APPEND);
    }
}

// Закрываем элемент catalog
$xmlWriter->endElement();

// Скидываем последние изменения в файл
file_put_contents('../output/large.xml', $xmlWriter->flush(true), FILE_APPEND);
