<?php
/**
 * Created for internal use only
 * User: Felix Antufiev [felix@oneway.su]
 */

ini_set('memory_limit', '32M');

$exampleData = [
    'prefix' => ['Great', 'Fantastic', 'Unbelievable', 'Exquisite', 'Wonderful'],
    'name' => ['sneakers', 'boots', 'felt boots', 'shoes', 'loafers'],
    'developer' => ['Adidas', 'Gucci', 'Gosha Rubchinskiy', 'Pharrell Williams', 'Balenciaga'],
    'color' => ['Red', 'Orange', 'Yellow', 'Blue', 'Pink'],
    'size' => [38, 39, 40, 41, 42],
    'price' => ['32 000 RUB', '6 000 RUB', '13 500 RUB', '4 900 RUB', '12 990 RUB'],
];

function getRandomProduct(array &$exampleData)
{
    return array_map(function ($item) {
        return $item[rand(0, count($item) - 1)];
    }, $exampleData);
}


// С помощью DOM

// Создаём документ и корневой элемент каталог
$xmlFile = new DOMDocument('1.0', 'UTF-8');
$catalogNode = $xmlFile->appendChild($xmlFile->createElement('catalog', ''));

// Пишем товары
for ($i = 1; $i < 5; $i++) {
    $arProduct = getRandomProduct($exampleData);

    // Создаём ноду product
    $productNode = $catalogNode->appendChild($xmlFile->createElement('product'));

    // Создаём ноды name, color, size, price
    $productNode->appendChild($xmlFile->createElement('name', "{$arProduct['prefix']} {$arProduct['name']} by {$arProduct['developer']}"));
    $productNode->appendChild($xmlFile->createElement('color', $arProduct['color']));
    $productNode->appendChild($xmlFile->createElement('size', $arProduct['size']));
    $productNode->appendChild($xmlFile->createElement('price', $arProduct['price']));
}

// Надо выставить перед сохранением,
// чтобы получить красивенький файл с отступами
$xmlFile->preserveWhiteSpace = true;
$xmlFile->formatOutput = true;

file_put_contents('../output/smallByDOM.xml', $xmlFile->saveXML());


// С помощью SimpleXML

// Создаём документ и корневой элемент каталог
$xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><catalog/>');

// Пишем товары
for ($i = 1; $i < 5; $i++) {
    $arProduct = getRandomProduct($exampleData);

    // Создаём ноду product
    $productNode = $xml->addChild('product');

    $productNode->addChild('name', "{$arProduct['prefix']} {$arProduct['name']} by {$arProduct['developer']}");
    $productNode->addChild('name', $arProduct['color']);
    $productNode->addChild('name', $arProduct['size']);
    $productNode->addChild('name', $arProduct['price']);
}

// Отформатировать вывод средствами SimpleXML нельзя
$xml->saveXML('../output/smallBySimpleXML.xml');

// Зато можно скормить XML DOM'у и сохранить им
$dom = dom_import_simplexml($xml)->ownerDocument;
$dom->formatOutput = true;
file_put_contents('../output/smallBySimpleXMLWithDOM.xml', $dom->saveXML());

